from django.db import models


class Server(models.Model):
    instance_name = models.CharField("Instance Name", max_length=255)
    instance_id = models.CharField("Instance ID", max_length=255)
    operating_system = models.CharField("Operating System", max_length=255)
    private_ip = models.TextField(blank=True, null=True)
    vcpu =  models.IntegerField(blank=True, null=True)
    memory = models.IntegerField(blank=True, null=True)
    created_at = models.TextField(blank=True, null=True)
    expires_at = models.TextField(blank=True, null=True)
    instance_type = models.CharField("Instance Type", max_length=255)
    instance_type_family = models.CharField("Instance Type Family", max_length=255)
    billing_type = models.CharField("Billing Type", max_length=255)
    users = models.CharField("Users", max_length=255)
    services_name = models.CharField("Services Name", max_length=255)
    apps_name = models.CharField("Apps Name", max_length=255)
    createdAt = models.DateTimeField("Created At", auto_now_add=True)

    def __str__(self):
        return self.instance_name
        