from rest_framework import serializers
from .models import Server


class ServerSerializer(serializers.ModelSerializer):

  class Meta:
    model = Server
    fields = ('pk', 'instance_name', 'instance_id', 'operating_system', 'private_ip', 'vcpu', 'memory', 'created_at', 'expires_at', 'instance_type', 'instance_type_family', 'billing_type', 'users', 'services_name', 'apps_name')
      