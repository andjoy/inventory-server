import React, { Component } from 'react';
import ServersService from './ServersService';

const serversService = new ServersService();

class ServersList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      servers: [],
      nextPageURL: ''
    };
    this.nextPage = this.nextPage.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    var self = this;
    serversService.getServers().then(function (result) {
      console.log(result);
      self.setState({ servers: result.data, nextPageURL: result.nextlink })
    });
  }
  handleDelete(e, pk) {
    var self = this;
    serversService.deleteServer({ pk: pk }).then(() => {
      var newArr = self.state.servers.filter(function (obj) {
        return obj.pk !== pk;
      });

      self.setState({ servers: newArr })
    });
  }

  nextPage() {
    var self = this;
    console.log(this.state.nextPageURL);
    serversService.getServersByURL(this.state.nextPageURL).then((result) => {
      self.setState({ servers: result.data, nextPageURL: result.nextlink })
    });
  }
  render() {

    return (
      <div className="servers--list">
        <table className="table">
          <thead key="thead">
            <tr>
              <th>#</th>
              <th>Instance Name</th>
              <th>Instance ID</th>
              <th>Operating System</th>
              <th>Private IP</th>
              <th>vCPU</th>
              <th>Memory</th>
              <th>Created At</th>
              <th>Expires At</th>
              <th>Instance Type</th>
              <th>Instance Type Family</th>
              <th>Billing Type</th>
              <th>Users</th>
              <th>Services Name</th>
              <th>Apps Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.servers.map(c =>
              <tr key={c.pk}>
                <td>{c.pk}  </td>
                <td>{c.instance_name}</td>
                <td>{c.instance_id}</td>
                <td>{c.operating_system}</td>
                <td>{c.private_ip}</td>
                <td>{c.vcpu}</td>
                <td>{c.memory}</td>
                <td>{c.created_at}</td>
                <td>{c.expires_at}</td>
                <td>{c.instance_type}</td>
                <td>{c.instance_type_family}</td>
                <td>{c.billing_type}</td>
                <td>{c.users}</td>
                <td>{c.services_name}</td>
                <td>{c.apps_name}</td>
                <td>
                  <button onClick={(e) => this.handleDelete(e, c.pk)}> Delete</button>
                  <a href={"/server/" + c.pk}> Update</a>
                </td>
              </tr>)}
          </tbody>
        </table>
        <button className="btn btn-primary" onClick={this.nextPage}>Next</button>
      </div>
    );
  }
}
export default ServersList;