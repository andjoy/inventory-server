import axios from 'axios';
const API_URL = 'http://localhost:8001';

export default class ServersService {

  constructor() { }


  getServers() {
    const url = `${API_URL}/api/servers/`;
    return axios.get(url).then(response => response.data);
  }
  getServersByURL(link) {
    const url = `${API_URL}${link}`;
    return axios.get(url).then(response => response.data);
  }
  getServer(pk) {
    const url = `${API_URL}/api/servers/${pk}`;
    return axios.get(url).then(response => response.data);
  }
  deleteServer(server) {
    const url = `${API_URL}/api/servers/${server.pk}`;
    return axios.delete(url);
  }
  createServer(server) {
    const url = `${API_URL}/api/servers/`;
    return axios.post(url, server);
  }
  updateServer(server) {
    const url = `${API_URL}/api/servers/${server.pk}`;
    return axios.put(url, server);
  }
}