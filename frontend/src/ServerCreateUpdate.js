import React, { Component } from 'react';
import ServersService from './ServersService';

const serversService = new ServersService();

class ServerCreateUpdate extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { match: { params } } = this.props;
    if (params && params.pk) {
      serversService.getServer(params.pk).then((c) => {
        console.log(this.refs);
        this.refs.instanceName.value = c.instance_name;
        this.refs.instanceId.value = c.instance_id;
        this.refs.operatingSystem.value = c.operating_system;
        this.refs.privateIp.value = c.private_ip;
        this.refs.vCpu.value = c.vcpu;
        this.refs.memory.value = c.memory;
        this.refs.createdAt = c.created_at;
        this.refs.expiresAt = c.expires_at;
        this.refs.instanceType = c.instance_type;
        this.refs.instanceTypeFamily = c.instance_type_family;
        this.refs.billingType = c.billing_type;
        this.refs.users = c.users;
        this.refs.servicesName = c.services_name;
        this.refs.appsName = c.apps_name;
      })
    }
  }

  handleCreate() {
    serversService.createServer(
      {
        "instance_name": this.refs.instanceName.value,
        "instance_id": this.refs.instanceId.value,
        "operating_system": this.refs.operatingSystem.value,
        "private_ip": this.refs.privateIp.value,
        "vcpu": this.refs.vCpu.value,
        "memory": this.refs.memory.value,
        "created_at": this.refs.createdAt,
        "expires_at": this.refs.expiresAt,
        "instance_type": this.refs.instanceType,
        "instance_type_family": this.refs.instanceTypeFamily,
        "billing_type": this.refs.billingType,
        "users": this.refs.users,
        "services_name": this.refs.servicesName,
        "apps_name": this.refs.appsName
      }
    ).then((result) => {
      alert("Server created!");
    }).catch(() => {
      alert('There was an error! Please re-check your form.');
    });
  }
  handleUpdate(pk) {
    serversService.updateServer(
      {
        "pk": pk,
        "instance_name": this.refs.instanceName.value,
        "instance_id": this.refs.instanceId.value,
        "operating_system": this.refs.operatingSystem.value,
        "private_ip": this.refs.privateIp.value,
        "vcpu": this.refs.vCpu.value,
        "memory": this.refs.memory.value,
        "created_at": this.refs.created_at,
        "expires_at": this.refs.expires_at,
        "instance_type": this.refs.instance_type,
        "instance_type_family": this.refs.instanceTypeFamily,
        "billing_type": this.refs.billingType,
        "users": this.refs.users,
        "services_name": this.refs.servicesName,
        "apps_name": this.refs.appsName
      }
    ).then((result) => {
      console.log(result);
      alert("Server updated!");
    }).catch(() => {
      alert('There was an error! Please re-check your form.');
    });
  }
  handleSubmit(event) {
    const { match: { params } } = this.props;

    if (params && params.pk) {
      this.handleUpdate(params.pk);
    }
    else {
      this.handleCreate();
    }

    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label>
            Instance Name:</label>
          <input className="form-control" type="text" ref='instanceName' />

          <label>
            Instance ID:</label>
          <input className="form-control" type="text" ref='instanceId' />

          <label>
            Operating System:</label>
          <input className="form-control" type="text" ref='operatingSystem' />

          <label>
            Private IP:</label>
          <input className="form-control" type="text" ref='privateIp' />

          <label>
            vCPU:</label>
          <input className="form-control" type="text" ref='vCpu' />

          <label>
            Memory:</label>
          <input className="form-control" type="text" ref='memory' />

          <label>
            Created At:</label>
          <input className="form-control" type="text">{this.refs.createdAt}</input>

          <label>
            Expires At:</label>
          <input className="form-control" type="text" ref='expires_at' />

          <label>
            Instance Type:</label>
          <input className="form-control" type="text" ref='instanceType' />

          <label>
            Instance Type Family:</label>
          <input className="form-control" type="text" ref='instanceTypeFamily' />

          <label>
            Billing Type:</label>
          <input className="form-control" type="text" ref='billingType' />

          <label>
            Users:</label>
          <input className="form-control" type="text" ref='users' />

          <label>
            Services Name:</label>
          <input className="form-control" type="text" ref='servicesName' />

          <label>
            Apps Name:</label>
          <input className="form-control" type="text" ref='appsName' />


          <input className="btn btn-primary" type="submit" value="Submit" />
        </div>
      </form>
    );
  }
}

export default ServerCreateUpdate;